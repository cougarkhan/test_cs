﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.IO;

namespace json_test_3
{
    class Program
    {
        static void Main(string[] args)
        {
            string json = File.ReadAllText(@"C:\temp\test_2.json");
            JavaScriptSerializer ser = new JavaScriptSerializer();
            List<RootObject> tests = ser.Deserialize<List<RootObject>>(json);

            foreach (RootObject test in tests)
            {
                Console.WriteLine("Green {0} |  Grey {1}", test.test.green.color, test.test.grey.color);
            }
            Console.ReadLine();
        }
    }

    public class Green
    {
        public string color { get; set; }
    }

    public class Grey
    {
        public string color { get; set; }
    }

    public class Test
    {
        public Green green { get; set; }
        public Grey grey { get; set; }
    }

    public class RootObject
    {
        public Test test { get; set; }
    }
}
