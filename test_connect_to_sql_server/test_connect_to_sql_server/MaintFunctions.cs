﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test_connect_to_sql_server
{
    class MaintFunctions
    {
        // Create new Program Functions object
        private ProgramFunctions pf = new ProgramFunctions();
        private Timer_functions tf1 = new Timer_functions();
        private string time_result;
        private string[] code = { "INFO", "WARNING", "ERROR" };

        public bool clean_files(string path_to_folder, int age_days)
        {
            // Set Result Variable
            bool result = true;

            // If the path_to_folder does not exist then the function fails
            if (Directory.Exists(path_to_folder) == false)
            {
                Console.WriteLine("{0,-12}{1,-12}The folder {2} does not exist!", pf.format_logtime, code[2], path_to_folder);
                result = false;
                return result;
            }
            else
            {
                //Create a DirectoryInfo object of the directory to clean up
                DirectoryInfo folder = new DirectoryInfo(path_to_folder);

                // If the directory is readonly then the function fails
                if (folder.Attributes.HasFlag(FileAttributes.ReadOnly) == true)
                {
                    Console.WriteLine("{0,-12}{1,-12}The folder {2} is ReadOnly!", pf.format_logtime, code[2], path_to_folder);
                    result = false;
                    return result;
                }

                // File age Limit variable.  Determines how far back files will be kept
                DateTime file_age_limit = DateTime.Now.AddDays(-age_days);
                int files_removed_count = 0;

                // Measure results
                Timer_functions tf2 = new Timer_functions();
                tf2.set();


                foreach (FileInfo f in folder.GetFiles("*"))
                {
                    if (f.Exists == true)
                    {
                        if (f.CreationTime < file_age_limit)
                        {
                            tf1.set();
                            //f.Delete();
                            time_result = tf1.get();
                            if (f.Exists == false)
                            {
                                Console.WriteLine("{0,-12}{1,-12}Removing old file {2}...Success. {3}", pf.format_logtime, code[0], f.FullName, time_result);
                                files_removed_count++;
                            }
                            else
                            {
                                Console.WriteLine("{0,-12}{1,-12}Removing old file {2}...Failure. {3}", pf.format_logtime, code[2], f.FullName, time_result);
                            }
                        }
                    }
                }

                time_result = tf2.get();
                Console.WriteLine("{0,-12}{1,-12}Completed Cleaning Files in {2}.", pf.format_logtime, code[0], time_result);

                return result;
            }
        }

        public bool move_files(FileInfo source_file, string destination_folder)
        {
            bool result;
            string time_result;
            string destination_file = destination_folder + "\\" + source_file.Name;

            Console.WriteLine("{0,-12}{1,-12}Copying {2} -> {3}", pf.format_logtime, code[0], source_file, destination_file);
            tf1.set();
            source_file.CopyTo(destination_file);
            time_result = tf1.get();
            Console.WriteLine("{0,-12}{1,-12}File Copy Completed. {2}", pf.format_logtime, code[0], time_result);

            FileInfo file = new FileInfo(destination_file);
            if (file.Exists)
            {
                Console.WriteLine("{0,-12}{1,-12}Checking if Destination File Exists...Success.", pf.format_logtime, code[0]);
                result = true;
            }
            else
            {
                Console.WriteLine("{0,-12}{1,-12}Checking if Destination File Exists...Failure.", pf.format_logtime, code[2]);
                result = false;
            }

            return result;
        }
    }
}
