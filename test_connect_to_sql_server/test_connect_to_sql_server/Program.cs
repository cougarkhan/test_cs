﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;
using System.Collections;
using System.Diagnostics;
using System.ComponentModel;


namespace test_connect_to_sql_server
{
    class Program
    {
        static int Main(string[] args)
        {
            // Create new Program Functions object
            ProgramFunctions pf = new ProgramFunctions();

            //Create Timer Functions Class
            Timer_functions tf1 = new Timer_functions();

            // Create new Program Options object
            ProgramOptions po = new ProgramOptions("C:\\temp\\c");
            if (po.terminate == true)
            {
                Console.WriteLine("ProgramOptions Instance invalid.");
                return 10;
            }

            // Create new Maintenance Functions object
            MaintFunctions mtf = new MaintFunctions();

            // Create new DB Functions object
            DBFunctions dbf = new DBFunctions();

            // Create new Streamwriter object to output to log file
            string hostname = Environment.GetEnvironmentVariable("computername");
            string path_to_log = "C:\\temp\\c\\" + hostname + "-" + pf.format_datetime + ".log";
            
            FileStream output_stream = new FileStream(path_to_log, FileMode.OpenOrCreate, FileAccess.Write);
            StreamWriter writer = new StreamWriter(output_stream);
            TextWriter old_output = Console.Out;
            //Console.SetOut(writer);

            //Create new Server Connection object and Server object
            Server srv;
            ServerConnection srv_conn = new ServerConnection(po.instance);
            srv = new Server(srv_conn);
            srv.ConnectionContext.StatementTimeout = 3600;

            List<Database> database_list = new List<Database>();

            foreach (XElement db in po.databases)
            {
                Database database_item = new Database(srv, db.Value);
                database_item.Refresh();
                database_list.Add(database_item);
            }

            string[] code = { "INFO", "WARNING", "ERROR" };

            Console.WriteLine("*************************************");
            Console.WriteLine("Begining the Super Wicked Awesome Backup Script!");
            Console.WriteLine();
            Console.WriteLine("Start time: {0}", pf.format_datetime);
            Console.WriteLine("Username: {0}\\{1}", Environment.GetEnvironmentVariable("USERDOMAIN"), Environment.GetEnvironmentVariable("USERNAME"));
            Console.WriteLine("Machine: {0}", Environment.GetEnvironmentVariable("COMPUTERNAME"));
            Console.WriteLine("*************************************");
            Console.WriteLine();
            Console.WriteLine("{0,-12}{1,-12}Processing {2} SQL Version {3} {4}.", pf.format_logtime, code[0], po.instance, srv.Information.Version, srv.Information.Edition);

            Console.WriteLine();
            Console.WriteLine( "--------------------------------------------");
	        Console.WriteLine( "Skipped Operations...");
            Console.WriteLine("--------------------------------------------");
            Console.WriteLine();
            if (!po.cleanup_files_enabled) { Console.WriteLine("{0,-12}{1,-12}Skipping Cleaning up of the Backup Files.", pf.format_logtime, code[0]); }
            if (!po.check_integrity_enabled) { Console.WriteLine("{0,-12}{1,-12}tSkipping Table Integrity Checks.", pf.format_logtime, code[0]); }
            if (!po.index_defrag_enabled) { Console.WriteLine("{0,-12}{1,-12}Skipping Index Defragmenting.", pf.format_logtime, code[0]); }
            if (!po.update_statistics_enabled) { Console.WriteLine("{0,-12}{1,-12}Skipping Updating of Databse Statistics.", pf.format_logtime, code[0]); }
            if (!po.backup_database_enabled) { Console.WriteLine("{0,-12}{1,-12}Skipping Backing Up of Databases.", pf.format_logtime, code[0]); }
            if (!po.move_files_enabled) { Console.WriteLine("{0,-12}{1,-12}Skipping Move of Backup Files.", pf.format_logtime, code[0]); }

            if (po.cleanup_files_enabled)
            {
                Console.WriteLine();
                Console.WriteLine("--------------------------------------------");
                Console.WriteLine("Performing Cleanup Operations...");
                Console.WriteLine("--------------------------------------------");
                Console.WriteLine();
                mtf.clean_files("C:\\temp\\logs2", po.cleanup_files_age);
            }

            if (po.check_integrity_enabled)
            {
                Console.WriteLine();
                Console.WriteLine("--------------------------------------------");
                Console.WriteLine("Performing Integrity Checks...");
                Console.WriteLine("--------------------------------------------");
                Console.WriteLine();
            }

            if (po.index_defrag_enabled)
            {
                Console.WriteLine();
                Console.WriteLine("--------------------------------------------");
                Console.WriteLine("Performing Index Defragmentation...");
                Console.WriteLine("--------------------------------------------");
                Console.WriteLine();
                foreach (Database db in database_list)
                {
                    //db.Refresh();
                    foreach (Table tb in db.Tables)
                    {
                        dbf.index_defragmentaion(tb, srv);
                    }
                }
            }

            if (po.update_statistics_enabled)
            {
                Console.WriteLine();
                Console.WriteLine("--------------------------------------------");
                Console.WriteLine("Performing Update of Table Statistics...");
                Console.WriteLine("--------------------------------------------");
                Console.WriteLine();
                foreach (Database db in database_list)
                {
                    //db.Refresh();
                    foreach (Table tb in db.Tables)
                    {
                        dbf.update_statistics(tb);
                    }
                }
            }

            if (po.backup_database_enabled)
            {
                Console.WriteLine();
                Console.WriteLine("--------------------------------------------");
                Console.WriteLine("Performing Database Backups...");
                Console.WriteLine("--------------------------------------------");
                Console.WriteLine();

                string backup_folder = po.backup_database_location;

                foreach (Database db in database_list)
                {
                    db.Refresh();
                    dbf.backup_database(srv, db, backup_folder, po.cleanup_files_age);
                }

                //dbf.backup_database(srv, database_list, backup_folder, file_age_days);
            }


                      
            srv.ConnectionContext.StatementTimeout = 1200;
            Console.SetOut(old_output);
            writer.Close();
            output_stream.Close(); 
            Console.ReadKey();

            return 0;
        }

    }
}
