﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Diagnostics; 

namespace test_connect_to_sql_server
{
    class ProgramFunctions
    {
        public string format_datetime = (DateTime.Now.ToString("yyyyMMddHHmmss"));

        public string format_logtime = (DateTime.Now.ToString("HH:mm:ss"));
    }

    class Timer_functions
    {
        private Stopwatch sw;
        public TimeSpan ts;

        public Timer_functions()
        {
            sw = new Stopwatch();
        }

        public void set()
        {
            sw.Restart();
        }

        public string get()
        {
            sw.Stop();
            string elapsed_time;
            ts = sw.Elapsed;

            elapsed_time = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);

            return elapsed_time;
        }
    }
}
