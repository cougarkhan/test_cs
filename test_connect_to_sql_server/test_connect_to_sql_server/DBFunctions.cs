﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;
using System.Collections;
using System.Diagnostics;
using System.Collections.Generic;
using System.Data;

namespace test_connect_to_sql_server
{
    class DBFunctions
    {
        // Create new Program Functions object
        private ProgramFunctions pf = new ProgramFunctions();

        // Create new Timer Functions object
        private Timer_functions tf1 = new Timer_functions();
        private string time_result;

        // Error Codes
        private string[] code = { "INFO", "WARNING", "ERROR" };
         
        public bool backup_database(Server srv, Database db, string backup_folder, int days)
        {
            bool result = true;
            string db_folder = backup_folder + "\\" + db.Name;

            if (Directory.Exists(db_folder) == false)
            {
                Directory.CreateDirectory(db_folder);
                if (Directory.Exists(db_folder))
                {
                    Console.WriteLine("{0,-12}{1,-12}{2} does not exist.  Creating folder...Success", pf.format_logtime, code[1], db_folder);
                }
                else
                {
                    Console.WriteLine("{0,-12}{1,-12}{2} does not exist.  Creating folder...Failure", pf.format_logtime, code[2], db_folder);
                    result = false;
                    return result;
                }

            }

            //RecoveryModel recovery_model = db.DatabaseOptions.RecoveryModel;
            DeviceType device_type = DeviceType.File;
            BackupTruncateLogType backup_truncate_log_type = BackupTruncateLogType.Truncate;

            string backup_file = String.Format("{0}\\{1}\\{2}_db_{3}.bak", backup_folder, db.Name, db.Name, pf.format_datetime);
            tf1.set();
            Backup smo_backup = new Backup();
            time_result = tf1.get();
            if (smo_backup != null)
            {
                Console.WriteLine("{0,-12}{1,-12}Creating SQL Server Backup Object...Success. {2}", pf.format_logtime, code[0], time_result);
                result = true;
            }
            else
            {
                Console.WriteLine("{0,-12}{1,-12}Creating SQL Server Backup Object...Success. {2}", pf.format_logtime, code[2], time_result);
                Console.WriteLine("{0,-12}{1,-12}Failed to create SMO Backup object", pf.format_logtime, code[2]);
                result = false;
                return result;
            }

            smo_backup.Action = BackupActionType.Database;
            smo_backup.BackupSetDescription = "Full Backup of " + db.Name;
            smo_backup.BackupSetName = db.Name + "-" + pf.format_datetime + "-Backup";
            smo_backup.Database = db.Name;
            smo_backup.MediaDescription = "Disk";
            smo_backup.Incremental = false;
            smo_backup.ExpirationDate = DateTime.Now.AddDays(days);
            smo_backup.LogTruncation = backup_truncate_log_type;

            BackupDeviceItem backup_device_item = new BackupDeviceItem(backup_file, device_type);
            smo_backup.Devices.Add(backup_device_item);

            tf1.set();

            try
            {
                smo_backup.SqlBackup(srv);
                time_result = tf1.get();
                Console.WriteLine("{0,-12}{1,-12}Backing up Database {2} to {3}...Success. {4}", pf.format_logtime, code[0], db.Name, backup_file, time_result);
                result = true;
            }
            catch (Exception e)
            {
                time_result = tf1.get();
                Console.WriteLine("{0,-12}{1,-12}Backing up Database {2} to {3}...Failure. {4}", pf.format_logtime, code[2], db.Name, backup_file, time_result);
                Console.WriteLine("{0,-12}{1,-12}{2}\r\n\t\t{3}", pf.format_logtime, code[2], e.Message, e.InnerException);
                result = false;
                return result;
            }

            FileInfo file = new FileInfo(backup_file);

            if (file.Exists)
            {
                Console.WriteLine("{0,-12}{1,-12}{2} Exists.", pf.format_logtime, code[0], backup_file);
            }
            else
            {
                Console.WriteLine("{0,-12}{1,-12}{2} does not Exist.", pf.format_logtime, code[2], backup_file);
            }

            return result;
        }

        public bool update_statistics(Table table)
        {
            bool result = true;
            
            try
            {
                Console.WriteLine("{0,-12}{1,-12}{2} Statistics are being updated...", pf.format_logtime, code[0], table.Name);
                table.UpdateStatistics(StatisticsTarget.All, StatisticsScanType.Percent, 50);
                Console.WriteLine("{0,-12}{1,-12}Completed updating statistics for {2}...", pf.format_logtime, code[0], table.Name);
            }
            catch (Exception e)
            {
                Console.WriteLine("{0,-12}{1,-12}Caught Exception...", pf.format_logtime, code[2]);
                Console.WriteLine("{0,-12}{1,-12}{2}...", pf.format_logtime, code[2], e);
                result = false;
                return result;
            }

            return result;
        }

        public bool index_defragmentaion(Table table, Server srv)
        {
            bool result = true;

            int index_was_reorganized = 0;
            int page_size = 100;
            int server_version = srv.Version.Major;
            float fragmentation;
            
            DataTable index_fragmentation_list = new DataTable();
            index_fragmentation_list = table.EnumFragmentation();

            foreach (DataRow dr in index_fragmentation_list.Rows)
            {
                string index_name = Convert.ToString(dr["Index_Name"]);
                int pages = Convert.ToInt16(dr["Pages"]);
                
                if (server_version < 9.0)
                {
                    fragmentation = Convert.ToSingle(dr["LogicalFragmentation"]);
                }
                else
                {
                    fragmentation = Convert.ToSingle(dr["AverageFragmentation"]);
                }

                

                if (fragmentation > 5.00 && pages > page_size && !(index_name.Equals("syscolumns", StringComparison.Ordinal)) && !(index_name.Equals("syscomments", StringComparison.Ordinal)))
                {
                    Console.WriteLine("{0,-12}{1,-12}{2,-6}{3,-40}{4,-25}{5,-11}{6:N2}", pf.format_logtime, code[1], "ID: " + dr["Index_ID"], "Name: " + index_name, "Type: " + dr["IndexType"], "Pages: " + pages, "Avg_Frag: " + fragmentation);
                    Console.WriteLine("{0,-12}{1,-12}{2} is {3} fragmented and will be rebuilt.", pf.format_logtime, code[1], index_name, fragmentation);
                }





            }
            

            return result;
        }
    }
}
