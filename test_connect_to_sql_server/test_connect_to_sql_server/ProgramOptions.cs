﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Collections;
using System.Diagnostics;

namespace test_connect_to_sql_server
{
    class ProgramOptions
    {
        public string instance;
        public IEnumerable databases;
        public bool cleanup_files_enabled;
        public string cleanup_files_source_folder;
        public int cleanup_files_age;
        public bool check_integrity_enabled;
        public bool index_defrag_enabled;
        public bool update_statistics_enabled;
        public bool backup_database_enabled;
        public string backup_database_location;
        public bool move_files_enabled;
        public string move_files_source_folder;
        public string move_files_target_folder;
        private bool failure = false;

        public ProgramOptions()
        {
        }

        public ProgramOptions(string path_to_config_file)
        {
            string hostname = Environment.GetEnvironmentVariable("computername");
            string file_path = null;
            
            if (File.Exists(path_to_config_file))
            {
                file_path = path_to_config_file;
            }
            else if (Directory.Exists(path_to_config_file))
            {
                string temp_file = path_to_config_file + "\\" + hostname + ".xml";
                file_path = temp_file;
            }
            else
            {
                file_path = null;
                failure = true;
            }

            if (file_path != null)
            {
                XDocument xdoc = XDocument.Load(file_path);

                instance = Convert.ToString(xdoc.Descendants("instance").Elements("name").ElementAt(0).Value);
                databases = xdoc.Descendants("databases").Elements("name");//.Cast<string>().ToArray();
                cleanup_files_enabled = Convert.ToBoolean(xdoc.Descendants("cleanupFiles").Elements("enabled").ElementAt(0).Value);
                cleanup_files_source_folder = Convert.ToString(xdoc.Descendants("cleanupFiles").Elements("sourcefolder").ElementAt(0).Value);
                cleanup_files_age = Convert.ToInt16(xdoc.Descendants("cleanupFiles").Elements("fileagedays").ElementAt(0).Value);
                check_integrity_enabled = Convert.ToBoolean(xdoc.Descendants("checkIntegrity").Elements("enabled").ElementAt(0).Value);
                index_defrag_enabled = Convert.ToBoolean(xdoc.Descendants("indexDefrag").Elements("enabled").ElementAt(0).Value);
                update_statistics_enabled = Convert.ToBoolean(xdoc.Descendants("updateStatistics").Elements("enabled").ElementAt(0).Value);
                backup_database_enabled = Convert.ToBoolean(xdoc.Descendants("backupDatabase").Elements("enabled").ElementAt(0).Value);
                backup_database_location = Convert.ToString(xdoc.Descendants("backupDatabase").Elements("location").ElementAt(0).Value);
                move_files_enabled = Convert.ToBoolean(xdoc.Descendants("moveFiles").Elements("enabled").ElementAt(0).Value);
                move_files_source_folder = Convert.ToString(xdoc.Descendants("moveFiles").Elements("sourcefolder").ElementAt(0).Value);
                move_files_target_folder = Convert.ToString(xdoc.Descendants("moveFiles").Elements("targetfolder").ElementAt(0).Value);
            }
        }

        public bool terminate
        {
            get
            {
                return failure;
            }
            set
            {
                failure = value;
            }
        }
    }
}
