﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Renci.SshNet;
using Renci.SshNet.Common;
using System.Security.Cryptography;


namespace test_ssh
{
    class Program
    {
        public static string ReadPassword()
        {
            Stack<string> pass = new Stack<string>();

            for (ConsoleKeyInfo consKeyInfo = Console.ReadKey(true);
              consKeyInfo.Key != ConsoleKey.Enter; consKeyInfo = Console.ReadKey(true))
            {
                if (consKeyInfo.Key == ConsoleKey.Backspace)
                {
                    try
                    {
                        Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
                        Console.Write(" ");
                        Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
                        pass.Pop();
                    }
                    catch (InvalidOperationException ex)
                    {
                        /* Nothing to delete, go back to previous position */
                        Console.SetCursorPosition(Console.CursorLeft + 1, Console.CursorTop);
                    }
                }
                else
                {
                    Console.Write("*");
                    pass.Push(consKeyInfo.KeyChar.ToString());
                }
            }
            String[] password = pass.ToArray();
            Array.Reverse(password);
            return string.Join(string.Empty, password);
        }



        static void Main(string[] args)
        {
            string host = "bis-nagios.bussops.ubc.ca";
            string username = "sysadmin";
            //int port = 22;
            Console.Write("Enter password: ");
            string password = ReadPassword();

            //Rfc2898DeriveBytes KeyBytes = new Rfc2898DeriveBytes("helloworld"

            var keyboardAuthMethod = new KeyboardInteractiveAuthenticationMethod(username);

            keyboardAuthMethod.AuthenticationPrompt += delegate(Object senderObject, AuthenticationPromptEventArgs eventArgs)
            {
                foreach (var prompt in eventArgs.Prompts)
                {
                    if (prompt.Request.Equals("Password: ", StringComparison.InvariantCultureIgnoreCase))
                    {
                        prompt.Response = password;

                    }
                }
            };

            var passwordAuthMethod = new PasswordAuthenticationMethod(username, password);

            var connectInfo = new ConnectionInfo(host, username, passwordAuthMethod, keyboardAuthMethod);

            SshClient serverConnection = new SshClient(connectInfo);

            serverConnection.Connect();
            var cmd = serverConnection.RunCommand("ls /etc");
            Console.WriteLine(cmd.Result);
            Console.ReadKey();

        }
    }
}
