﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.DirectoryServices.AccountManagement;

namespace test_validate_auth
{
    class Program
    {
        static void Main(string[] args)
        {
            PrincipalContext insPC = new PrincipalContext(ContextType.Domain, "EAD", "DC=EAD,DC=UBC,DC=CA");
            bool result = insPC.ValidateCredentials("bis-svc-revcord", "2aedYdrZybjSRffcy1hF");
            Console.WriteLine("User is {0}", result);
            Console.ReadKey();
        }
    }
}
