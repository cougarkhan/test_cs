﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Management.Automation;


namespace test_get_process_cmdlet
{
    [Cmdlet(VerbsCommon.Get, "fuck")]
    public class Class1 : Cmdlet
    {
        ;
        protected override void ProcessRecord()
        {
            Process[] processes = Process.GetProcesses();
            WriteObject(processes, true);
        }
    }

    [Cmdlet(VerbsCommon.Get, "tinker")]
    public class Class2 : Cmdlet
    {
        [Parameter(Mandatory = true)]
        public string Item
        {
            get { return item; }
            set { item = value; }
        }
        private string item;

        protected override void ProcessRecord()
        {
            WriteObject(item);
        }
    }
}
