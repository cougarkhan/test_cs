﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.IO;

namespace json_test_2
{
    class Program
    {
        static void Main(string[] args)
        {
            string json = File.ReadAllText(@"C:\temp\test.json");
            JavaScriptSerializer ser = new JavaScriptSerializer();
            List<Item> items = ser.Deserialize<List<Item>>(json);

            foreach (Item item in items)
            {
                Console.WriteLine("ID: {0}.  Name: {1}.  Age: {2}.  Enabled: {3}.", item.id, item.Name, item.Age, item.Enabled);
            }
            Console.ReadLine();
        }
    }

    public class Item
    {
        public int id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public bool Enabled { get; set; }
    }
}
