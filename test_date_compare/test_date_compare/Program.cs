﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace test_date_compare
{
    class Program
    {
        static void Main(string[] args)
        {
            string folder = "C:\\temp\\age";
            DirectoryInfo dir = new DirectoryInfo(folder);

            FileInfo[] files = dir.GetFiles();

            double age = 14;
            bool result;
            DateTime limit = DateTime.Now;

            foreach (FileInfo file in files)
            {
                Console.WriteLine("\nFile {0}", file.Name);
                Console.WriteLine("Limit {0}", limit);
                Console.WriteLine("Creation Time {0}", file.CreationTime);
                Console.WriteLine("Diffrence {0}", (limit - file.CreationTime).TotalDays);
                result = (limit - file.CreationTime).TotalDays > age;
                Console.WriteLine(result);
            }

            Console.ReadKey();
        }
    }
}
