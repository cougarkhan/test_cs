﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using System.Web.Script.Serialization;
using System.IO;

namespace json_test
{
    class Program
    {
        static void Main(string[] args)
        {
            string json = File.ReadAllText(@"E:\powershell\Branch01\SQL\sql_maintenance\etc\settings.json");
            JavaScriptSerializer ser = new JavaScriptSerializer();
            List<RootObject> config = ser.Deserialize<List<RootObject>>(json);
            ser.Deserialize(json, );

            foreach (RootObject c in config)
            {
                Console.WriteLine("Instance Name : " + c.sqlserver.instance.name);
                foreach (string db in c.sqlserver.databases.name)
                {
                    Console.WriteLine("Database Name : " + db);
                }
                Console.WriteLine("Cleanup Files : " + c.sqlserver.options.cleanup_files.enabled);
            }
            Console.ReadLine();
        }
    }

    public class Instance
    {
        public string name { get; set; }
    }

    public class Databases
    {
        public List<string> name { get; set; }
    }

    public class CleanupFiles
    {
        public bool enabled { get; set; }
        public string source_folder { get; set; }
        public int file_age_days { get; set; }
    }

    public class CheckIntegrity
    {
        public bool enabled { get; set; }
    }

    public class IndexDefrag
    {
        public bool enabled { get; set; }
    }

    public class UpdateStatistics
    {
        public bool enabled { get; set; }
    }

    public class BackupDatabase
    {
        public bool enabled { get; set; }
        public string destination_folder { get; set; }
    }

    public class MoveFiles
    {
        public bool enabled { get; set; }
        public string source_folder { get; set; }
        public string destination_folder { get; set; }
    }

    public class Options
    {
        public CleanupFiles cleanup_files { get; set; }
        public CheckIntegrity check_integrity { get; set; }
        public IndexDefrag index_defrag { get; set; }
        public UpdateStatistics update_statistics { get; set; }
        public BackupDatabase backup_database { get; set; }
        public MoveFiles move_files { get; set; }
    }

    public class Sqlserver
    {
        public Instance instance { get; set; }
        public Databases databases { get; set; }
        public Options options { get; set; }
    }

    public class RootObject
    {
        public Sqlserver sqlserver { get; set; }
    }
}
