﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Renci.SshNet;
using Renci.SshNet.Sftp;
using System.IO;

namespace test_ssh_2
{
    class Program
    {
        static void Main(string[] args)
        {
            double downtime = new Double();

            if (args.Length > 1)
            {
                Console.WriteLine("Too many arguments");
                Environment.Exit(1);
            }
            else if (args.Length < 1)
            {
                downtime = 900;
            }
            else
            {
                bool val = System.Text.RegularExpressions.Regex.IsMatch(args[0], "[a-zA-Z]+$");

                if (!val)
                {
                    downtime = Convert.ToDouble(args[0]);
                }
                else
                {
                    Console.WriteLine("Argument not numeric");
                    Environment.Exit(1);
                }
            }
            string machine_name = Environment.MachineName;
            string author_name = Environment.UserName;
            PrivateKeyFile key = new PrivateKeyFile(@"E:\powershell\Branch01\Nagios\remote");
            
            var client = new SshClient("137.82.196.70", "nagremote", key);
            client.Connect();
            
            string comments = "Restart Server";
            string command_file = "/usr/local/nagios/var/rw/nagios.cmd";
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            TimeSpan diff = DateTime.Now.ToUniversalTime() - origin;
            double start_time = Math.Floor(diff.TotalSeconds);
            double end_time = start_time + downtime;
            string argument = "/usr/bin/printf \"[%lu] SCHEDULE_HOST_DOWNTIME;" + machine_name + ";" + start_time + ";" + end_time + ";1;0;" + downtime + ";" + author_name + ";" + comments + "\" " + start_time + " | tee " + command_file;

            SshCommand cmd = client.CreateCommand(argument);
            Console.WriteLine(cmd.Execute());

            Console.ReadKey();

            //using (var client = new SftpClient("127.0.0.1", "mike", "S1mpl32"))
            //{
            //    client.Connect();
            //    using (var file = File.OpenRead(@"E:\Image.iso"))
            //    {
            //        client.UploadFile(file,"test.iso");
            //    }
            //    client.Disconnect();
            //}

            client.Disconnect();
            client.Dispose();
        }
    }
}
