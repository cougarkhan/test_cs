﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace test_sql_connection
{
    class Program
    {
        static void Main(string[] args)
        {
            string data_source = null;

            if (args.Length > 0)
            {
                data_source = args[0];
            }

            if (data_source.Equals(null))
            {
                data_source = "bops-d3-tst.bussops.ubc.ca";
            }

            Console.WriteLine(data_source);
            Console.ReadKey();

            SqlConnection sqlConn = new SqlConnection("Data Source=" + data_source + ";Integrated Security=true;Database=D3__UBC;");
            
            try
            {
                sqlConn.Open();

                SqlCommand sqlComm = new SqlCommand("Select * from ADMIN_ADMIN", sqlConn);

                SqlDataReader sqlReader = null;

                sqlReader = sqlComm.ExecuteReader();

                while (sqlReader.Read())
                {
                    Console.WriteLine(sqlReader.ToString());
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

        }
    }
}
