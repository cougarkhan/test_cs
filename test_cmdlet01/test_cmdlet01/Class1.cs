﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;

namespace test_cmdlet01
{
    [Cmdlet(VerbsCommunications.Send, "Greeting")]
    public class Class1 : Cmdlet
    {
        [Parameter(Mandatory = true)]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        private string name;

        protected override void ProcessRecord()
        {
            WriteObject("Hello " + name + "!");
            
        }
    }
}
