﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test_sharepoint_api
{
    class allServers
    {
        private string LINKTITLE;
        public string linktitle
        {
	        get { return LINKTITLE; }
	        set { LINKTITLE = value; }
        }
        private string OS;
        public string os
        {
	        get { return OS; }
	        set { OS = value; }
        }
        private string DNSNAME;
        public string dnsname
        {
	        get { return DNSNAME; }
	        set { DNSNAME = value; }
        }
        private string NETINTERFACE;
        public string netinterface
        {
            get { return NETINTERFACE; }
            set { NETINTERFACE = value; }
        }
        private string IPADDRESS;
        public string ipaddress
        {
	        get { return IPADDRESS; }
	        set { IPADDRESS = value; }
        }
        private string ENVIRONMENT;
        public string environment
        {
	        get { return ENVIRONMENT; }
	        set { ENVIRONMENT = value; }
        }
        private string INFRASTRUCTURE;
        public string infrastructure
        {
	        get { return INFRASTRUCTURE; }
	        set { INFRASTRUCTURE = value; }
        }
        private string LOCATION;
        public string location
        {
	        get { return LOCATION; }
	        set { LOCATION = value; }
        }
        private string DEPT;
        public string dept
        {
	        get { return DEPT; }
	        set { DEPT = value; }
        }
        private string REMARKS;
        public string remarks
        {
	        get { return REMARKS; }
	        set { REMARKS = value; }
        }
        public allServers()
        {

        }
    }
}
