﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint.Client;
using SP = Microsoft.SharePoint.Client;
using System.Net;
using System.Net.Security;

namespace test_sharepoint_api
{
    class Program
    {
        static void Main(string[] args)
        {
            string siteUrl = "https://bis.share.ubc.ca/AdminOps/";

            ServicePointManager.ServerCertificateValidationCallback = new
            RemoteCertificateValidationCallback
            (
               delegate { return true; }
            );
            
            ClientContext clientContext = new ClientContext(siteUrl);
            Web oWebsite = clientContext.Web;
            List allServers = oWebsite.Lists.GetByTitle("AllServers");
            //ListCollection collList = oWebsite.Lists;

            //try
            //{
            //    clientContext.Load(collList);//, lists => lists.Include(list => list.Title));
            //    clientContext.ExecuteQuery();

            //    foreach (SP.List oList in collList)
            //    {
            //        Console.WriteLine("Title: {0,-50} Created: {1}", oList.Title, oList.Created.ToString());
            //    }
            //}
            //catch (SystemException e)
            //{
            //    Console.WriteLine(e);
            //}

            List<allServers> serverList = new List<allServers>();

            try
            {
                CamlQuery query = new CamlQuery();
                //query.ViewXml = "<View><Query><Where><Contains><FieldRef Name='Name1'/><Value Type='Text'></Value></Contains></Where></Query></View>";
                //query.ViewXml = "<Query><OrderBy><FieldRef Name='Link Title'/></OrderBy></Query>";
                query.ViewXml = "<Query><OrderBy><FieldRef Name='LinkTitle' /><FieldRef Name='OS' /><FieldRef Name='DNS_NAME' /><FieldRef Name='Interface' /><FieldRef Name='IP_ADDRESS' /><FieldRef Name='ENVIRONMENT' /><FieldRef Name='INFRASTRUCTURE' /><FieldRef Name='LOCATION' /><FieldRef Name='Dept' /></OrderBy></Query>";
                ListItemCollection collListItem = allServers.GetItems(query);

                clientContext.Load(collListItem);
                clientContext.ExecuteQuery();

                if (collListItem.Count == 0)
                {
                    Console.WriteLine("No items found.");
                }
                else
                {
                    
                    foreach (ListItem targetListItem in collListItem)
                    {
                        allServers server = new allServers();
                        server.linktitle = targetListItem["Title"].ToString();
                        //server.os = targetListItem["OS"].ToString();
                        //server.dnsname = targetListItem["DNS_NAME"].ToString();
                        //server.netinterface = targetListItem["Interface"].ToString();
                        //server.ipaddress = targetListItem["IP_ADDRESS"].ToString();
                        //server.environment = targetListItem["ENVIRONMENT"].ToString();
                        //server.infrastructure = targetListItem["INFRASTRUCTURE"].ToString();
                        //server.location = targetListItem["LOCATION"].ToString();
                        //server.dept = targetListItem["Dept"].ToString();
                        //server.remarks = targetListItem["Remarks"].ToString();

                        serverList.Add(server);
                    }
                }

                foreach (allServers server in serverList)
                {
                    Console.WriteLine(server.linktitle);
                }

            }
            catch (SystemException e)
            {
                Console.WriteLine(e);
            }

        }
    }
}
